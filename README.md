# Dicepass

Dicepass generates passphrases that are easy to remember, but hard for a
computer to guess. It uses dice or a PRNG, and a word list provided by the
[EFF][EFF-link].

## Usage
Run either:
- `dicepass` with no arguments (use this method if using dice)
- `dicepass -l [length]` (uses the default passphrase format: lowercase)
- `dicepass -f [format]`
- `dicepass -l [length] -f [format]`

###### Length:
`5-12` words long

###### Format:
- `camelcase`: ExampleExample
- `lowercase`: example example (default)
- `uppercase`: EXAMPLE EXAMPLE
- `mixedcase`: exampleEXAMPLE 
- `snakecase`: example_example 
- `snakecase-capitalised`: Example_Example
- `strict`: Example_Example7 (For when a number and symbol is required)
- `random`: Any of the above except "strict"


Copy the generated passphrase into a password manager or keep on a piece of
paper until memorised.

## License

Copyright &copy; 2019 [Max](https://www.maxg.dev)

[![GNU GPLv3 logo][GPLv3-logo]](https://www.gnu.org/licenses/gpl-3.0.en.html)

Thanks to the [EFF][EFF-link] for the diceware list.

[EFF-link]: https://www.eff.org
[GPLv3-logo]: https://www.gnu.org/graphics/gplv3-127x51.png
