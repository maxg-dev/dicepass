#!/usr/bin/env python3
#
# Copyright (C) 2019 Max <max@maxg.dev>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

"""A CLI for dicepass.

This module displays a diceware-style passphrase created from command line
arguments, if present. Otherwise, it will ask the user for the required
information. Passphrases are between 5 and 12 words in length (inclusive).
"""

import re
import sys

from argparse import ArgumentParser
from core import Passphrase, generate_rolls

FORMATS = (
    ('c', 'camelcase'), ('l', 'lowercase'), ('u', 'uppercase'),
    ('m', 'mixedcase'), ('s', 'snakecase'), ('sc', 'snakecase-capitalised'),
    ('st', 'strict'), ('r', 'random')
)


def clean_input(string):
    return string.lower().strip()


def get_method_name(fmt):
    for tup in FORMATS:
        if fmt in tup:
            return tup[1].replace('-', '_')
    return None


def is_using_dice():
    while True:
        decision = clean_input(input(
            'Would you like to use dice to generate your passphrase? '
            'Enter y or N: '
        ))
        if decision == 'y':
            return True
        elif decision == 'n' or not decision:
            return False
        print('Please try again.')


def get_length_from_user():
    while True:
        pass_length = clean_input(input(
            'Enter a passphrase length between 5 and 12 inclusive (the number of words in the passphrase): '
        ))
        print(pass_length)
        if re.match('([5-9]|1[012])$', pass_length):
            return int(pass_length)
        print('Error: input must be a digit between 5 and 12 inclusive.')


def get_rolls_from_user(length):
    rolls = []
    while len(rolls) < length:
        roll = clean_input(input('Enter the result of 5 (6-sided) dice rolls: '))
        if re.match('[1-6]{5}$', roll):
            rolls.append(roll)
            print('Roll saved.')
        else:
            print(
                'Error: input must be 5 digits in length and each digit must '
                'be between 1 and 6 inclusive.'
            )
    return rolls


def get_method_name_from_user():
    while True:
        info = (
            'Passphrases can be formatted as: (c) camelcase, (l) lowercase, '
            '(u) uppercase, (m) mixedcase, (s) snakecase, (sc) '
            'snakecase-capitalised, (st) strict, or any one of these (r) '
            'random. Enter a format: '
        )
        chosen_fmt = clean_input(input(info))
        fmt = get_method_name(chosen_fmt)
        if fmt:
            return fmt
        print(f'Error: {fmt} is not a valid format.')


def argparse():
    ap = ArgumentParser()
    ap.add_argument(
        '-l', '--length', type=int, choices=range(5, 13),
        default=6, required=False
    )
    fmt_choices = []
    for tup in FORMATS:
        for x in tup:
            fmt_choices.append(x)
    ap.add_argument(
        '-f', '--format', choices=fmt_choices, default='lowercase',
        required=False
    )
    return vars(ap.parse_args())


def main():
    if len(sys.argv) > 1:
        args = argparse()
        length = args['length']
        method_name = get_method_name(clean_input(args['format']))
        rolls = generate_rolls(length)

    else:
        method_name = get_method_name_from_user()
        if is_using_dice():
            rolls = get_rolls_from_user(get_length_from_user())
        else:
            rolls = generate_rolls(get_length_from_user())

    result = getattr(Passphrase(rolls), method_name)()
    print(result)


if __name__ == '__main__':
    main()
