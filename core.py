# Copyright (C) 2019 Max <max@maxg.dev>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

import json
import os

from random import choice as insecure_choice
from secrets import SystemRandom


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
_sr = SystemRandom()


def randint_str(a, b):
    return str(_sr.randint(a, b))


class Passphrase:
    """A diceware-style passphrase."""
    _default_dict = None

    def __init__(self, rolls, diceware_dict=None):
        """Creates a new Passphrase object

        :param rolls: a list of rolls. Each roll must be a string of 5 digits
                     between 11111 and 66666 inclusive, with each digit being 
                     between 1 and 6 inclusive.
        :param dict diceware_dict: a diceware list. One provided by the EFF is
                                   used by default.
        """
        self._diceware_dict = diceware_dict
        self._passphrase = [self.diceware_dict[x] for x in rolls]

    @property
    def diceware_dict(self):
        if self._diceware_dict:
            return self._diceware_dict
        if type(self)._default_dict:
            return type(self)._default_dict
        diceware_file = os.path.join(BASE_DIR, 'diceware.json')
        with open(diceware_file, 'r') as f:
            type(self)._default_dict = json.load(f)
            return type(self)._default_dict

    @diceware_dict.setter
    def diceware_dict(self, diceware_dict):
        self._diceware_dict = diceware_dict

    def camelcase(self):
        """Returns a passphrase with the following format: ExampleExample."""
        return ''.join([x.capitalize() for x in self._passphrase])

    def lowercase(self):
        """Returns a passphrase with the following format: example example."""
        return ' '.join(self._passphrase)

    def uppercase(self):
        """Returns a passphrase with the following format: EXAMPLE EXAMPLE."""
        return ' '.join([x.upper() for x in self._passphrase])

    def mixedcase(self):
        """Returns a passphrase with the following format: EXAMPLEexample."""
        return ''.join([v.upper() if i % 2 == 1 else v
                        for i, v in enumerate(self._passphrase)])

    def snakecase(self):
        """Returns a passphrase with the following format: example_example."""
        return '_'.join(self._passphrase)

    def snakecase_capitalised(self):
        """Returns a passphrase with the following format: Example_Example."""
        return '_'.join([x.capitalize() for x in self._passphrase])

    def strict(self):
        """Returns a passphrase suitable for websites that have strict requirements.

        It contains uppercase and lowercase letters, digits, and underscores.
        """
        return self.snakecase_capitalised() + randint_str(1, 9)

    def random(self):
        """Returns the passphrase with a randomly selected format."""
        return insecure_choice([self.camelcase, self.lowercase, self.uppercase,
                                self.mixedcase, self.snakecase,
                                self.snakecase_capitalised, ])()


def generate_rolls(length: int):
    """Returns a list of dice rolls.

    Each element in the list represents 5 dice rolls, with each roll being 
    a digit between 1 and 6 inclusive. Therefore, each list element is between 
    11111 and 66666 inclusive.

    :param int length: the length of the list.
    :returns list rolls: the generated rolls.
    """
    def rolls():
        return ''.join([randint_str(1, 6) for _ in range(5)])

    return [rolls() for _ in range(length)]
