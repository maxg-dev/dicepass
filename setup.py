import os

from setuptools import setup, find_packages

setup(
    name='dicepass',
    version="2.0.0",
    description='A diceware-style passphrase generator',
    keywords='diceware passphrase',
    author='Max G',
    author_email='max@maxg.dev',
    license='GPLv3',
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'dicepass=cli:main',
        ]
    },
)
